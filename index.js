/*
	Activity:

	1. Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
    2. Create a class constructor able to receive 3 arguments
        It should be able to receive 3 strings and a number
        Using the this keyword assign properties:
        username, 
        role, 
        guildName,
        level 
  assign the parameters as values to each property.
  Create 2 new objects using our class constructor.
  This constructor should be able to create Character objects.



	Pushing Instructions:

	Create a git repository named S24.
	Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	Add the link in Boodle.

*/

//Solution: 

/*Debug*/
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}


/*Debug and Update*/

function introduce(student){

	console.log("Hi! I'm " + student.name + "." + " I am " + student.age + " years old.")
	console.log("I study the following courses: " + student.classes)

}

introduce(student1);
introduce(student2);


let getCube = (num) => num**3;
let cubed = getCube(5);
console.log(cubed);

let numArr = [15,16,32,21,21,2]

let displayNum = (num) => {

	num.forEach(function(num){
		console.log(num);
	})
		
}
displayNum(numArr);

let numsSquared = numArr.map((num) => num ** 2);
console.log(numsSquared);

/*2. Class Constructor*/

class User {
	constructor(username,role,guildname,level){
		this.username = username;
		this.role = role;
		this.guildName = guildname;
		this.level = level;
	}
}

let user1 = new User("javaman","tank","Aegis","1");
let user2 = new User("rhasta","support","Wards","25");
console.log(user1);
console.log(user2);